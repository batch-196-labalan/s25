let sample1 = `

{

	"name": "Katniss Everdeen",
	"age": 20,
	"address": {
		"city": "Kansas City",
		"state": "Kansas"
	}

  }

`;

console.log(sample1);


//JSON to JS Object //can't use array methods
//NEED TO CONVERT JSON INTO AN ARRAY/OBJECT TO APPLY METHOD
// To convert use : (JSON.parse());

console.log(JSON.parse(sample1));



let sampleArr = `

[
	{

	"email": "jasonNewsted@gmail.com",
	"password": "iplaybass1234",
	"isAdmin": false
	},	
	{

	"email": "larsDrum@gmail.com",
	"password": "metallicaMe80",
	"isAdmin": true

}
  ]

`;

console.log(sampleArr);


let parseSampleArr = JSON.parse(sampleArr);
console.log(parseSampleArr);

console.log(parseSampleArr.pop());

console.log(parseSampleArr);


//to update it back in JSON format

sampleArr = JSON.stringify(parseSampleArr);
console.log(sampleArr);



// MINI ACTIVITY

let jsonArr = `

	[
		"pizza",
		"hamburger",
		"spaghetti",
		"shanghai",
		"hotdog stick on a pineapple",
		"pancit bihon"

	]

`;

console.log(jsonArr);

let newJsonArr = JSON.parse(jsonArr);
console.log(newJsonArr);

console.log(newJsonArr.pop());
console.log(newJsonArr);


console.log(newJsonArr.push("Ice Cream"));


jsonArr = JSON.stringify(newJsonArr);
console.log(jsonArr);




